const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Club = new Schema({
    name: {
        type: String,
        required: true,
        text: true
    },
    logo: {
        type: String
    },
    country: {
        type: String,
        required: true
    },
    players: {
        type: [{
            firstname: String,
            lastname: String,
            number: String,
            role: String
        }],
        required: true
    }
});

module.exports = mongoose.model('club', Club);