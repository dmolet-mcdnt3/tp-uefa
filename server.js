const express = require('express');
const hbs = require('hbs');
const app = express();
const Club = require('./models/club.model');
require("./db/mongodb-connection");

const PORT = 5000;

hbs.registerHelper('comp', (val1, val2, returnedStr) => {
  if (val1 === val2) return returnedStr;
})

hbs.registerHelper('ifeq', function(val1, val2, options) {
  if(val1 === val2) return options.fn(this);
});

app.set('view engine', 'hbs');

app.use(express.static(__dirname + '/public'));

app.get("/clubs", (req, res) => {
    const conditions = {};
    if (req.query.country) {
        conditions.country = req.query.country.toUpperCase();
    }

    if (req.query.name) {
        conditions.$text = { $search: req.query.name };
    }

    Club.find(conditions)
    .then(clubs => {
        res.json(clubs);
    });
});

app.listen(PORT, () => {
  console.log('Serveur écoutant le port 5000...');
})

