const mongoose = require('mongoose');
const cheerio = require('cheerio');
const Club  = require('../models/club.model');
const { getUrlHtml, downloadUrl } = require("./utils");
require('../db/mongodb-connection');

const clubs_url = "https://fr.uefa.com/uefachampionsleague/season=2019/clubs/";

(async function main() {

    console.log("Fetching club list");
    const clubUrls = await getClubs(clubs_url);

    const clubs = await Promise.all(
        clubUrls.map(async clubUrl => {
            console.log("Fetching club " + clubUrl);
            const club = await getClub(clubUrl);
            console.log(`Saving ${club.name}'s logo...`);
            await saveClubLogo(club.logo);
            console.log(`Done fetching ${club.name}`);
            return makeClubModel(club);
        })
    );

    console.log("Inserting into database...");
    await Club.insertMany(clubs);
    console.log("Done !");

    mongoose.disconnect();
})();

async function getClub(clubUrl) {
    const clubHtml = await getUrlHtml(clubUrl);
    const $ = cheerio.load(clubHtml);

    const clubCountry = $(".team-country-name").text();
    const logoElem = $(".club-logo");
    const clubName = logoElem.attr("title");
    const logoLink = logoElem.attr("style").match(/url\('(.+)'\)/)[1];

    const playerElems = $(".squad--team-player")
    const players = new Array(playerElems.length);
    playerElems.each((i, el) => {
        const $$ = $(el);
        const firstname = $$.find(".squad--player-name-name").text();
        const lastname = $$.find(".squad--player-name-surname").text();
        const role = $$.find(".squad--player-role").text();
        const number = $$.find(".squad--player-num").text();

        players[i] = {
            firstname,
            lastname,
            role,
            number
        };
    });

    return {
        name: clubName,
        country: clubCountry,
        logo: logoLink,
        players
    };
}

async function saveClubLogo(url) {
    const logoFileName = url.substring(url.lastIndexOf('/')+1);
    const logoWritePath = __dirname + "/../public/img/" + logoFileName;
    await downloadUrl(url, logoWritePath);
}

async function makeClubModel(club) {
    const logoFileName = club.logo.substring(club.logo.lastIndexOf('/')+1);

    return new Club({
        name: club.name,
        country: club.country,
        logo: logoFileName,
        players: club.players
    });
}

async function getClubs(clubsUrl) {
    const clubListHtml = await getUrlHtml(clubsUrl);
    const $ = cheerio.load(clubListHtml);

    const teamLinks = $(".team a");
    const teamUrls = new Array(teamLinks.length);
    teamLinks.each((i, elem) => {
        teamUrls[i] = "https://fr.uefa.com" + elem.attribs.href;
    });

    return teamUrls;
}