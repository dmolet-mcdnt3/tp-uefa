const https = require('https');
const fs = require('fs');
const path = require('path');

async function getUrlHtml(url) {
    return new Promise((resolve, reject) => {
        https.get(url, res => {
            res.on("error", err => reject(err));

            let body = "";
            res.on('data', (chunk) => {
                body += chunk;
            });

            res.on('end', () => {
                resolve(body);
            })
        });
    });
}

async function downloadUrl(url, outputPath) {
    const dirname = path.dirname(outputPath);
    await fs.promises.mkdir(dirname, { recursive: true })
    .then(new Promise((resolve, reject) => {
        https.get(url, res => {
            res.on("error", err => reject(err));

            res.pipe(fs.createWriteStream(outputPath));

            res.on('end', () => {
                resolve();
            })
        });
    }));
}

module.exports = {
    getUrlHtml,
    downloadUrl
}